<div class="ecImage-metabox">
	<div class="ecImage-formrow">
		<label><?php print ecImageI18n::get('label_taxonomie'); ?></label>
		<sub><?php print ecImageI18n::get('help_taxonomie'); ?></sub>
		<input type="text" name="ecimage-taxonomie" value="<?php print $args->taxonomie ?>" />
	</div>
	<div class="ecImage-formrow">
		<label><?php print ecImageI18n::get('label_width'); ?></label>
		<input type="number" step="1" name="ecimage-width" value="<?php print $args->width ?>" /> px
	</div>
	<div class="ecImage-formrow">
		<label><?php print ecImageI18n::get('label_height'); ?></label>
		<input type="number" step="1" name="ecimage-height" value="<?php print $args->height ?>" /> px
	</div>
	<div class="ecImage-formrow">
		<label><?php print ecImageI18n::get('label_scaling'); ?></label>
		<select name="ecimage-scaling">
			<option value="contain" <?php print ($args->scaling === 'contain')?'selected':'' ?> ><?php print ecImageI18n::get('label_scaling_contain'); ?></option>
			<option value="cover" <?php print ($args->scaling === 'cover')?'selected':'' ?> ><?php print ecImageI18n::get('label_scaling_cover'); ?></option>
			<option value="scale" <?php print ($args->scaling === 'scale')?'selected':'' ?> ><?php print ecImageI18n::get('label_scaling_scale'); ?></option>
		</select>
	</div>
</div>