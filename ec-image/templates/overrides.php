<div class="wrap">
	<h1><?php print esc_html( get_admin_page_title() ); ?></h1>
	<form method="post" action="options.php">
		<?php 
			settings_fields( 'ecimage_overrides' );
			do_settings_sections( 'ecimage_overrides' );
			submit_button();
		?>
	</form>
</div>