<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ecImageProvider
 *
 * @author Felix
 */
class ecImageProvider {
	
	const CACHE_URL = ecImage_upload_url;
	
	const TMP_PATH = ecImage_tmp_dir;
	const CACHE_PATH = ecImage_upload_dir;
	
	const SUPPORTED_MIMETYPE = array('image/jpeg','image/png','image/gif');
	
	public static function get () {
		
		if (!self::checkCachDir()) {
			self::notFound('Cache dir not Writeable');
		}
		
		if (!self::checkTmpDir()) {
			self::notFound('Tmp dir not Writeable');
		}
		
		$dirname = $_GET['d'];
		$filename = $_GET['f'];
		$size = $_GET['s'];
		$extension = $_GET['e'];
		
		$imageID = ecImageDatabase::getImageIdByFilename("{$dirname}{$filename}.{$extension}");
		
		if (empty($imageID)) {
			self::notFound('Attachment not Found');
		}
		
		$ecImageEditor = new ecImageEditor($imageID, $size);
		
		if (!$ecImageEditor->stream()) {
			self::notFound($ecImageEditor->error);
		}
		
		exit;
		
	}
	
	public static function checkCachDir () {
		
		if (!is_dir(self::CACHE_PATH)) {
			if (!mkdir(self::CACHE_PATH, 0775, true)) {
				return false;
			}
		}
		if (!is_writable(self::CACHE_PATH)) {
			return false;
		}
		
		return true;
	}
	
	public static function checkTmpDir () {
		
		if (!is_dir(self::TMP_PATH)) {
			if (!mkdir(self::TMP_PATH, 0775, true)) {
				return false;
			}
		}
		if (!is_writable(self::TMP_PATH)) {
			return false;
		}
		
		return true;
	}
	
	public static function notFound ($msg) {
		header("HTTP/1.0 404 Not Found");
		error_log(print_r(debug_backtrace(),true));
		if (!empty($msg)) {
			print $msg;
		}
		exit();
	}
	
}
