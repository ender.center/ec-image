<?php

class ecImage {
	
	public static function getImageUrl ($imageID, $sizeID) {
		
		$media = wp_get_attachment_metadata($imageID, true);
		if (empty($media)) {
			return false;
		}
		
		$size = new ecImageSize($sizeID);
		if (empty($size->ID)) {
			return false;
		}
		
		$pathinfo = pathinfo($media['file']);
		return ecImageProvider::CACHE_URL . "{$pathinfo['dirname']}/{$pathinfo['filename']}_{$size->taxonomie}.{$pathinfo['extension']}";
		
	}
	
	public static function getImageTag ($imageID, $size) {
		
		$media = wp_get_attachment_metadata($imageID, true);
		if (empty($media)) {
			return false;
		}
		
		$oSize = new ecImageSize($size);
		if (empty($oSize->ID)) {
			return false;
		}
		
		$pathinfo = pathinfo($media['file']);
		$src = ecImageProvider::CACHE_URL . "{$pathinfo['dirname']}/{$pathinfo['filename']}_{$oSize->taxonomie}.{$pathinfo['extension']}";
		
		return "<img src=\"{$src}\" alt=\"{$media['image_meta']['title']}\" title=\"{$media['image_meta']['title']}\" />"; //width=\"{$oSize->width}\" height=\"{$oSize->height}\" 
		
	}
	
	public static function getSizes ($imageID, $fullPath = true, $override = array()) {
		
		$sizes = array();
		
		$mimetype = get_post_mime_type($imageID);
		
		// check if mimetype is supported
		if (!in_array($mimetype, ecImageProvider::SUPPORTED_MIMETYPE)) {
			return $sizes;
		}
		
		$media = wp_get_attachment_metadata($imageID, true);
		
		foreach (ecImageDatabase::getSizeIDs() AS $sizeID) {
			
			$size = new ecImageSize($sizeID);
			
			if (empty($size->ID)) {
				continue;
			}
			
			if ($size->width > $media['width'] && $size->height > $media['height']) {
				continue; // image too small for this size
			}
			
			$pathinfo = pathinfo($media['file']);
			if ($fullPath) {
				$src = ecImageProvider::CACHE_URL . "{$pathinfo['dirname']}/{$pathinfo['filename']}_{$size->taxonomie}.{$pathinfo['extension']}";
			} else {
				$src = "{$pathinfo['filename']}_{$size->taxonomie}.{$pathinfo['extension']}";
			}
			
			$editor = new ecImageEditor($imageID, $size->ID);
			
			$scaledSize = $editor->getSize();
			
			$sizes['ecimage_'.$size->taxonomie] = array(
				'file' => $src,
				'width' => intval($scaledSize['width']),
				'height' => intval($scaledSize['height']),
				'mime-type' => $mimetype
			);
			
			$o = array_keys($override, $size->ID);
			foreach ($o AS $slug) {
				$sizes[$slug] = $sizes['ecimage_'.$size->taxonomie];
			}
		}
		
		return $sizes;
		
	}
	
}