<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ecImageI18n
 *
 * @author Felix
 */
class ecImageI18n {
	
	const TEXT_DOMAIN = 'ec-image';
	
	public static function get ($id) {
		if (isset(self::${$id})) {
			return __(self::${$id}, self::TEXT_DOMAIN);
		} else {
			return $id;
		}
	}
	
	/* General Strings */
	private static $name = 'ec Image Sizes';
	private static $singular_name = 'ec Image Size';
	
	/* Labels */
	private static $label_focus_width = 'Focus position on width';
	private static $label_focus_height = 'Focus position on height';
	private static $label_focus_crosshair = 'show Crosshair';
	private static $label_cachesize = 'Cache Size';
	private static $label_clearcache = 'Clear Cache';
	private static $label_overrides = 'Overrides';
	private static $label_overrides_tabel_size = 'size';
	private static $label_overrides_tabel_nogen = 'prevent generation';
	private static $label_overrides_tabel_override = 'override size';
	private static $label_overrides_nooverride = 'no override';
	private static $label_taxonomie = 'Taxonomie';
	private static $label_width = 'Width';
	private static $label_height = 'Height';
	private static $label_scaling = 'Scaling';
	private static $label_scaling_contain = 'Contain';
	private static $label_scaling_cover = 'Cover';
	private static $label_scaling_scale = 'Scale';
	private static $label_user = 'Username';
	private static $label_licensekey = 'License Key';
	private static $label_checklicense = 'Check License';
	private static $label_licensestatus = 'License Status';
	private static $label_status_active = 'Active';
	private static $label_status_inactive = 'Inactive';
	private static $label_status_expires = 'Expires';
	private static $label_status_free = 'Free';
	
	/* section labels */
	private static $section_license = 'License';
	private static $section_general = 'General';
	
	/* help texts */
	private static $help_focus = 'Set the Point the Image should be cropped to.';
	private static $help_clearcache = 'Select this to Clear the Cache on save.';
	private static $help_taxonomie = 'Must be unique as it is used as a suffix to identify the image size';
	private static $help_checklicense = 'Select this to Check license on save.';
	
	/* menu entries */
	private static $menu_options = 'ec Image';
	private static $menu_override = 'ec Image Override';
	
	/* page titles */
	private static $title_override = 'ec Image Override';
	
	/* info text */
	private static $info_section_license_1 = 'to get your subscription license-key you have to Register your website <a href="https://ender.center/user/">here</a>';
	private static $info_section_license_2 = 'You will also find there informations about pricing';
	
	/* notices */
	private static $notice_notsaved_license = 'Data was not saved! Your License is not active. Please activate your License.';
	private static $notice_wrong_posttype = 'Wrong post-type! this should not have happened...';
	private static $notice_noright = 'You have no right to do that!';
	private static $notice_premium_feature = 'This is a subscription feature, you need an active license to save this settings';
	private static $notice_license_expires = 'Your license is due to expire, please renew your licence.';
	private static $notice_license_inactive = 'Your licence in inactive, please renew your licence.';
	private static $notice_license_free = 'Your on a free Trail, to use all features please get your subscription license.';
	private static $notice_cron_error = 'The License authentification failed. Please check your data and try again later.';
	
	/* snippets */
	private static $snippet_important = 'Important';
	private static $snippet_expires = 'expires';
	
}
