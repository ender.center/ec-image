<?php

class ecImageDatabase {
	
	public static function getSizeByID ($sizeID) {
		
		if (empty($sizeID) || get_post_type($sizeID) !== ecImageSize::$post_type) {
			return false;
		}
		
		$data = ecImageSize::getDefaults();
		
		$data->ID = $sizeID;
		$data->label = get_the_title( $sizeID );
		
		if (!empty($tmp = get_post_meta( $sizeID, 'ecimage-taxonomie', true ))) {
			$data->taxonomie = $tmp;
		} else {
			$data->taxonomie = sanitize_title( $data->label );
		}
		if (!empty($tmp = get_post_meta( $sizeID, 'ecimage-width', true ))) {
			$data->width = $tmp;
		}
		if (!empty($tmp = get_post_meta( $sizeID, 'ecimage-height', true ))) {
			$data->height = $tmp;
		}
		if (!empty($tmp = get_post_meta( $sizeID, 'ecimage-scaling', true ))) {
			$data->scaling = $tmp;
		}
		
		return $data;
		
	}
	
	public static function getSizeIDs () {
		
		return get_posts(array(
			'numberposts' => -1,
			'post_type' => ecImageSize::$post_type,
			'post_status' => array('publish'),
			'fields' => 'ids'
		));
		
	}
	
	public static function getSizeByTaxonomie ($sizeTaxonomie) {
		
		$args = array(
			'post_type'		=>	ecImageSize::$post_type,
			'meta_query'	=>	array(
				array(
					'key' => 'ecimage-taxonomie',
					'value' => $sizeTaxonomie
				)
			)
		);
		$query = new WP_Query( $args );
		if( $query->have_posts() ) {
			$query->the_post();
			$post_id = get_the_ID();
		}
		wp_reset_postdata();
		
		return self::getSizeByID($post_id);
		
	}
	
	public static function getImageIdByFilename ($attached_file) {
		global $wpdb;
		
		$post_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attached_file ) );
		
		return $post_id;
		
	}
	
	public static function getFocusPoint ($imageID) {
		
		$data = get_post_meta( $imageID, 'ecimage-focus', true );
		if (empty($data)) {
			$data = array(50,50);
		}
		
		return $data;
		
	}
	
	public static function getSizeCount () {
		 global $wpdb;
		 $query = "SELECT COUNT( * ) AS num_posts FROM {$wpdb->posts} WHERE post_type = %s";
		 $result = $wpdb->get_results( $wpdb->prepare( $query, ecImageSize::$post_type ));
		 if (!empty($result)) {
			 return $result[0]->num_posts;
		 }
		 return 0;
	}
	
}