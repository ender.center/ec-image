<?php

class ecImageSettings {
	
	public static function init () {
		// add Fields for Metadata to Edit Page
		add_action( 'add_meta_boxes_ecimage_size', array('ecImageSettings','addSizeMetaBox'));
		// handle metadata when post gets saved
		add_action( 'save_post_ecimage_size', array('ecImageSettings','onSizeSave'), 11, 3);
		// add Columns to Size overview
		add_filter( 'manage_ecimage_size_posts_columns', array('ecImageSettings','customColumnsInit') );
		// print Data for Custom Columns
		add_action( 'manage_ecimage_size_posts_custom_column' , array('ecImageSettings','customColumnsData'), 10, 2 );
		// make the columns sortable
		add_filter( 'manage_edit-ecimage_size_sortable_columns', array('ecImageSettings','customColumnsSortable') );
		// provide data for Columns Sort
		add_action( 'pre_get_posts', array('ecImageSettings','customColumnsSortData') );
		// register settings for settings Page
		add_action ('admin_init', array('ecImageSettings', 'registerSettings') );
	}
	
	public static function registerPages () {
		add_options_page( 'ec Image Settings', ecImageI18n::get('menu_options'), 'manage_options', 'ecimage_settings', array('ecImageSettings', 'printSettings') );
		add_submenu_page( 
				'edit.php?post_type='.ecImageSize::$post_type, 
				ecImageI18n::get('title_override'), 
				ecImageI18n::get('menu_override'), 
				'manage_options', 
				'ecimage_override', 
				array('ecImageSettings', 'printOverrides')
		);
	}
	
	public static function registerSettings () {
		register_setting( 'ecimage_settings', 'ecimage_cachesize' );
		register_setting( 'ecimage_settings', 'ecimage_clearcache', array('ecImageSettings','onClearcache') );
		add_settings_section( 'ecimage_section_general', ecImageI18n::get('section_general'), array('ecImageSettings','sectionSettings'), 'ecimage_settings' );
		ecImageSettingsFields::addSettingsField('cachesize', ecImageI18n::get('label_cachesize'), 'settings', 'general');
		ecImageSettingsFields::addSettingsField('clearcache', ecImageI18n::get('label_clearcache'), 'settings', 'general');
		self::registerSettingsWorkaround();
		
		register_setting( 'ecimage_overrides', 'ecimage_overrides' );
		add_settings_section( 'ecimage_section_overrides', '', array('ecImageSettings','sectionOverrides'), 'ecimage_overrides' );
		ecImageSettingsFields::addSettingsField('overrides', ecImageI18n::get('label_overrides'), 'overrides', 'overrides');
	}
	
	public static function registerSettingsWorkaround () {
	}
	
	/*
	 * Function that echos out any content at the top of the section (between heading and fields).
	 */
	public static function sectionSettings() {
		
	}
	
	/*
	 * Function that echos out any content at the top of the section (between heading and fields).
	 */
	public static function sectionLicense() {
		print '<p>'.ecImageI18n::get('info_section_license_1').'</p>';
		print '<p>'.ecImageI18n::get('info_section_license_2').'</p>';
	}
	
	/*
	 * Function that echos out any content at the top of the section (between heading and fields).
	 */
	public static function sectionOverrides() {
		
	}
	
	public static function printSettings () {
		if ( current_user_can( 'manage_options' ) ) {
			self::showTemplate('settings');
		}
	}
	
	public static function printOverrides () {
		if ( current_user_can( 'manage_options' ) ) {
			self::showTemplate('overrides');
		}
	}
	
	public static function showTemplate ($name, $args = array()) {
		include(ecImage_base_dir . "templates/$name.php");
	}
	
	public static function addSizeMetaBox () {
		add_meta_box(
			'ecimage-size-meta',
			'ecImage Meta',
			array('ecImageSettings','printSizeMetaBox'),
			'ecimage_size',
			'normal',
			'high'
		);
	}
	
	public static function printSizeMetaBox () {
		global $post;
		$ecImageSize = new ecImageSize($post->ID);
		self::showTemplate('size',$ecImageSize);
	}
	
	public static function onClearcache ($val) {
		if (! empty($val)) {
			ecImageAdmin::clearCache();
			return time();
		}
		return get_option( 'ecimage_clearcache', 0 );
	}
	
	public static function onSizeSave ($postID, $post, $update) {
		
		if (empty($postID) || get_post_type($postID) !== ecImageSize::$post_type) {
			ecImageNotice::addError(ecImageI18n::get('notice_wrong_posttype'));
			return false;
		}
		
		if ( ! current_user_can( 'edit_post', $postID ) ) {
			ecImageNotice::addError(ecImageI18n::get('notice_noright'));
			return false;
		}
		
		$old = ecImageDatabase::getSizeByID( $postID );
		
		if (!$update) {
			$default = ecImageSize::getDefaults();
			$taxonomie = $default->taxonomie;
			$width = $default->width;
			$height = $default->height;
			$scaling = $default->scaling;
		} else {
			$width = intval( $_POST['ecimage-width'] );
			$height = intval( $_POST['ecimage-height'] );
			$taxonomie = str_replace( '_', '-', sanitize_title( sanitize_text_field( $_POST['ecimage-taxonomie'] ) ) );
			if (empty($taxonomie)) {
				$taxonomie = str_replace( '_', '-', sanitize_title( get_the_title( $postID ) ) );
			}
			$scaling = sanitize_text_field( $_POST['ecimage-scaling'] );
		}
		
		// save width
		if ($width > 0) {
			update_post_meta( $postID, 'ecimage-width', $width );
		}
		// save height
		if ($height > 0) {
			update_post_meta( $postID, 'ecimage-height', $height );
		}
		
		// save new Taxonomie
		update_post_meta( $postID, 'ecimage-taxonomie', $taxonomie );
		
		// save new scaling
		if (!empty($scaling)) {
			update_post_meta( $postID, 'ecimage-scaling', $scaling );
		}
		
		// clear cache if critical values changed
		if ($update && ($old->taxonomie != $taxonomie || $old->width != $width || $old->height != $height || $old->scaling != $scaling)) {
			ecImageAdmin::clearCache($old->taxonomie);
		}
		
	}
	
	public static function customColumnsInit ($columns) {
		
		$date = $columns['date'];
		unset($columns['date']);
		
		$columns['taxonomie'] = 'Taxonomie';
		$columns['scaling'] = 'Scaling';
		$columns['width'] = 'Width';
		$columns['height'] = 'Height';
		$columns['date'] = $date;
		
		return $columns;
		
	}
	
	public static function customColumnsSortable ($columns) {
		
		$columns['taxonomie'] = 'taxonomie';
		$columns['scaling'] = 'scaling';
		$columns['width'] = 'width';
		$columns['height'] = 'height';
		
		return $columns;
		
	}
	
	public static function customColumnsSortData ($query) {
		
		if ( !is_admin() ) {
			return;
		}
		$orderby = $query->get( 'orderby' );
		
		switch ($orderby) {
			case 'taxonomie' :
				$query->set( 'meta_key', 'ecimage-taxonomie' );
				$query->set( 'orderby', 'meta_value' );
			break;
			case 'width' :
				$query->set( 'meta_key', 'ecimage-width' );
				$query->set( 'orderby', 'meta_value_num' );
			break;
			case 'height' :
				$query->set( 'meta_key', 'ecimage-height' );
				$query->set( 'orderby', 'meta_value_num' );
			break;
			case 'scaling' :
				$query->set( 'meta_key', 'ecimage-scaling' );
				$query->set( 'orderby', 'meta_value' );
			break;
		}
		
	}
	
	public static function customColumnsData ($column, $postID) {
		
		switch ($column) {
			case 'taxonomie' :
				print get_post_meta( $postID, 'ecimage-taxonomie', true );
			break;
			case 'width' :
				print get_post_meta( $postID, 'ecimage-width', true );
			break;
			case 'height' :
				print get_post_meta( $postID, 'ecimage-height', true );
			break;
			case 'scaling' :
				print get_post_meta( $postID, 'ecimage-scaling', true );
			break;
		}
		
	}
	
}