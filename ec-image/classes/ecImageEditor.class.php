<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ecImageEditor
 *
 * @author Felix
 */

class ecImageEditor {
	
	public $error = false;
	
	private $noSave = false;
	
	private $media;
	private $size;
	
	private $srcFile;
	private $dstFile;
	private $tmpFile;
	
	private $mimeType;
	
	public function __construct($imageID, $size) {
		
		$this->media = wp_get_attachment_metadata($imageID, true);
		$this->media['ecimage-focus'] = ecImageDatabase::getFocusPoint($imageID);
		$this->size = new ecImageSize($size);
		
		$uploadDir = wp_get_upload_dir();
		$this->srcFile = "{$uploadDir['basedir']}/{$this->media['file']}";
		
		$pathinfo = pathinfo($this->media['file']);
		$this->dstFile = rtrim(ecImageProvider::CACHE_PATH,'/') . "/{$pathinfo['dirname']}/{$pathinfo['filename']}_{$this->size->taxonomie}.{$pathinfo['extension']}";
		
		$this->tmpFile = rtrim(ecImageProvider::TMP_PATH,'/') . '/' . md5( $this->media['file'] . rand(0,time()) ) . ".{$pathinfo['extension']}";
		
		$this->mimeType = get_post_mime_type($imageID);
		
	}
	
	public function save () {
		
		$this->error = false;
		
		return $this->scale();
		
	}
	
	public function stream () {
		
		$this->error = false;
		
		// if img already exists just print it
		if (is_file($this->dstFile)) {
			$this->printImg();
			//if it comes here printing did fail
			return false;
		}
		
		// prevent images beeing saved from viewing them in admin page
		if (strpos(wp_get_referer(), '/wp-admin/') !== false) {
			$this->noSave = true;
		}
		
		$this->scale();
		
		if (!empty($this->error)) {
			return false;
		}
		
		// print img
		$this->printImg();
		//if it comes here printing did fail
		return false;
		
	}
	
	public function getSize () {
		
		$size = array(
			'width' => 0,
			'height' => 0,
		);
		
		if ($this->size->scaling == 'scale') {
			$copyData = $this->getCopyData($this->size->scaling, $this->media['width'], $this->media['height'], $this->size->width, $this->size->height);
			$size['width'] = $copyData['dst']['w'];
			$size['height'] = $copyData['dst']['h'];
		} else {
			$size['width'] = $this->size->width;
			$size['height'] = $this->size->height;
		}
		
		return $size;
		
	}
	
	private function printImg ($exit = true) {
		
		if (!is_file($this->dstFile)) {
			$this->error = 'Scaled image not Found';
			return false;
		}
		
		ob_end_clean();
		ob_start();
		
		$this->setContentType();
		
		header('Content-Length: ' . filesize($this->dstFile));
		readfile($this->dstFile);
		
		ob_end_flush();
		
		if ($exit) {
			exit;
		}
		
		return true;
		
	}
	
	private function printImgObj ($imgObj) {
		
		ob_end_clean();
		ob_start();

		$this->setContentType();

		switch ($this->mimeType) {
			case 'image/gif':
				imagegif($imgObj);
			break;
			case 'image/png':
				imagepng($imgObj);
			break;
			case 'image/jpeg':
				imagejpeg($imgObj);
			break;
		}

		ob_end_flush();

		exit();
		
	}
	
	private function setContentType () {
		
		switch ($this->mimeType) {
			case 'image/gif':
				header('Content-Type: image/jpeg');
				break;
			case 'image/png':
				header('Content-Type: image/jpeg');
				break;
			case 'image/jpeg':
				header('Content-Type: image/jpeg');
				break;
		}
		
	}
	
	private function scale () {
		
		$wpimg = wp_get_image_editor($this->srcFile);
		
		$copyData = $this->getCopyData($this->size->scaling, $this->media['width'], $this->media['height'], $this->size->width, $this->size->height);
		
		if ($this->size->scaling === 'cover') {
			$wpimg->crop($copyData['src']['x'], $copyData['src']['y'], $copyData['src']['w'], $copyData['src']['h'], $copyData['dst']['w'], $copyData['dst']['h']);
		} else {
			$wpimg->resize($copyData['dst']['w'], $copyData['dst']['h']);
			if ($copyData['dst']['x'] > 0 || $copyData['dst']['y'] > 0) {
				$wpimg->save($this->tmpFile);
				$result = $this->copyCentered();
				return $result;
			}
		}
		
		if ($this->noSave) {
			if ($wpimg->stream($this->mimeType) === true) {
				exit();
			}
		} else {
			$wpimg->save($this->dstFile, $this->mimeType);
		}
		
		return true;
		
	}
	
	private function copyCentered () {
		
		$imagesize = getimagesize($this->tmpFile);
		
		switch ($this->mimeType) {
			case 'image/gif':
				$imgSrc = imagecreatefromgif($this->tmpFile);
			break;
			case 'image/png':
				$imgSrc = imagecreatefrompng($this->tmpFile);
			break;
			case 'image/jpeg':
				$imgSrc = imagecreatefromjpeg($this->tmpFile);
			break;
			default:
				$this->error = "unsupported mime type: '{$this->mimeType}' for image {$this->media['ID']}";
				return false;
			break;
		}
		
		unlink($this->tmpFile);
		
		if (empty($imgSrc)) {
			$this->error = 'could not open image';
			return false;
		}
		
		$imgDst = imagecreatetruecolor($this->size->width, $this->size->height);
		imagealphablending($imgDst, true);
		
		//fill image with transparent white bg
		$fillColor = imagecolorallocatealpha($imgDst, 255, 255, 255, 127);
		imagefill($imgDst, 0, 0, $fillColor);
		imagecolordeallocate($imgDst,$fillColor);
		
		$w = $imagesize[0];
		$h = $imagesize[1];
		$x = round(($this->size->width - $w) / 2);
		$y = round(($this->size->height - $h) / 2);
		
		imagecopyresized( $imgDst, $imgSrc, $x, $y, 0, 0, $w, $h, $w, $h );
		
		imagedestroy($imgSrc);
		
		$pathinfo = pathinfo($this->dstFile);
		
		if (!is_dir($pathinfo['dirname'])) {
			mkdir($pathinfo['dirname'], 0775, true);
		}
		
		imagealphablending($imgDst, false);
		imagesavealpha($imgDst,true);
		
		if ($this->noSave) {
			$this->printImgObj($imgDst);
		}
		
		switch ($this->mimeType) {
			case 'image/gif':
				imagegif($imgDst, $this->dstFile);
			break;
			case 'image/png':
				imagepng($imgDst, $this->dstFile);
			break;
			case 'image/jpeg':
				imagejpeg($imgDst, $this->dstFile);
			break;
		}
		
		imagedestroy($imgDst);
		
		return true;
		
	}
	
	private function getCopyData ($mode, $srcWidth, $srcHeight, $dstWidth, $dstHeight) {
		$data = array(
			'src' => array(
				'x' => 0,
				'y' => 0,
				'w' => 0,
				'h' => 0,
				'r' => $srcWidth / $srcHeight
			),
			'dst' => array(
				'x' => 0,
				'y' => 0,
				'w' => 0,
				'h' => 0,
				'r' => $dstWidth / $dstHeight
			)
		);
		
		switch ($mode) {
			case 'cover':
				// start by setting width to src width and calc height for that width
				$tmpWidth = $srcWidth;
				$tmpHeight = $tmpWidth / $data['dst']['r'];
				// for cover width and height have to be >= dst image
				if ($tmpHeight > $srcHeight) {
					// if height too small go from height and calc width
					$tmpHeight = $srcHeight;
					$tmpWidth = $tmpHeight * $data['dst']['r'];
				}
				$tmpWidth = round($tmpWidth);
				$tmpHeight = round($tmpHeight);
				
				$data['src']['w'] = $tmpWidth;
				$data['src']['h'] = $tmpHeight;
				$data['dst']['w'] = $dstWidth;
				$data['dst']['h'] = $dstHeight;
				
				//$data['src']['x'] = ($srcWidth - $tmpWidth) / 2;
				//$data['src']['y'] = ($srcHeight - $tmpHeight) / 2;
				
				$focus = $this->media['ecimage-focus'];
				$focus['x'] = $srcWidth / 100 * $focus[0];
				$focus['y'] = $srcHeight / 100 * $focus[1];
				
				if ($focus['x'] < $tmpWidth / 2) {
					$data['src']['x'] = 0;
				} elseif ($srcWidth - $focus['x'] < $tmpWidth / 2) {
					$data['src']['x'] = $srcWidth - $tmpWidth;
				} else {
					$data['src']['x'] = $focus['x'] - $tmpWidth / 2;
				}
				
				if ($focus['y'] < $tmpHeight / 2) {
					$data['src']['y'] = 0;
				} elseif ($srcHeight - $focus['y'] < $tmpHeight / 2) {
					$data['src']['y'] = $srcHeight - $tmpHeight;
				} else {
					$data['src']['y'] = $focus['y'] - $tmpHeight / 2;
				}
				
			break;
			case 'contain':
			case 'scale':
				// start by setting width to dst width and calc height for that width
				$tmpWidth = $dstWidth;
				$tmpHeight = $tmpWidth / $data['src']['r'];
				// height and with have to be <= dst image
				if ($tmpHeight > $dstHeight) {
					// if height too big go from height and calc width
					$tmpHeight = $dstHeight;
					$tmpWidth = $tmpHeight * $data['src']['r'];
				}
				$tmpWidth = round($tmpWidth);
				$tmpHeight = round($tmpHeight);
				
				$data['src']['w'] = $srcWidth;
				$data['src']['h'] = $srcHeight;
				$data['dst']['w'] = $tmpWidth;
				$data['dst']['h'] = $tmpHeight;
				
				if ($mode == 'contain') {
					$data['dst']['x'] = ($dstWidth - $tmpWidth) / 2;
					$data['dst']['y'] = ($dstHeight - $tmpHeight) / 2;
				}
				
			break;
			default:
				$this->error = "Undefined Scalingmode: $mode";
				return false;
		}
		
		return $data;
	}
	
}
