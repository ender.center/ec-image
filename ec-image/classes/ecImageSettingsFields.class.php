<?php

/**
 * Description of ecImageFields
 *
 * @author Felix
 */
class ecImageSettingsFields {
	
	public static function addSettingsField ($slug, $label, $slugPage, $slugSection) {
		$slugUc = ucfirst($slug);
		add_settings_field ( 
			"ecimage_field_$slug", 
			$label, 
			array('ecImageSettingsFields',"print$slugUc"), 
			"ecimage_$slugPage", 
			"ecimage_section_$slugSection",
			array (
				'label_for' => "ecimage_$slug",
				'class' => "ecimage_field_$slug"
			)
		);
	}
	
	public static function printClearcache () {
		print '<label><input type="checkbox" name="ecimage_clearcache" value="1"> '.ecImageI18n::get('help_clearcache').'</label>';
	}
	
	public static function printCachesize () {
		print ecImageAdmin::getTempDirSize();
	}
	
	public static function printOverrides () {
		
		$overrideOptions = array();
		
		foreach (ecImageDatabase::getSizeIDs() AS $sizeID) {
			
			$size = new ecImageSize($sizeID);
			
			if (empty($size->ID)) {
				continue;
			}
			
			$overrideOptions[$size->ID] = "{$size->label} - {$size->width} x {$size->height}";
			
		}
		
		$overrides = get_option( 'ecimage_overrides', array() );
		
		print '<table><tr><th>'.ecimageI18n::get('label_overrides_tabel_size').
				'</th><th>'.ecimageI18n::get('label_overrides_tabel_nogen').
				'</th><th>'.ecimageI18n::get('label_overrides_tabel_override').'</th></tr>';
		
		foreach (get_intermediate_image_sizes() AS $size) {
			if (strpos($size, 'ecimage_') === 0) {
				continue;
			}
			print '<tr>';
			print "<th>$size</th>";
			$checked = '';
			if (!empty($overrides[$size]['nogen'])) {
				$checked = 'checked="checked"';
			}
			print "<td><input type='hidden' name='ecimage_overrides[$size][nogen]' value='0'><input type='checkbox' name='ecimage_overrides[$size][nogen]' $checked></td>";
			print "<td><select name='ecimage_overrides[$size][override]'><option value=''>".ecimageI18n::get('label_overrides_nooverride')."</option>";
			foreach ($overrideOptions AS $sizeID => $label) {
				$selected = '';
				if (!empty($overrides[$size]['override']) && $overrides[$size]['override'] == $sizeID) {
					$selected = 'selected="selected"';
				}
				print "<option value='$sizeID' $selected>$label</option>";
			}
			print "</select></td>";
			print '</tr>';
		}
		
		print '</table>';
		
	}
	
}
