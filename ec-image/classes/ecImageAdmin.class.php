<?php

class ecImageAdmin {
	
	private static $deactivation = false;
	
	public static function init () {
		
		add_action( 'init', array('ecImageAdmin', 'onInit') );
		add_action( 'after_setup_theme', array('ecImageAdmin', 'registerImageSizes'));
		add_action( 'wp_get_attachment_metadata', array('ecImageAdmin', 'attachmentMetadataHook'), 10, 2);
		add_action( 'mod_rewrite_rules', array('ecImageAdmin', 'modRewriteRulesHook'));
		
		if ( is_admin() ) {
			self::initAdmin();
		}
		
	}
	
	public static function initAdmin () {
		register_activation_hook( ecImage_base_dir . 'ec-image.php', array('ecImageAdmin', 'onActivation') );
		register_deactivation_hook( ecImage_base_dir . 'ec-image.php', array('ecImageAdmin', 'onDeactivation') );
		add_action( 'admin_menu', array('ecImageSettings', 'registerPages') );
		add_action( 'wp_ajax_ecImage_provider', array('ecImageProvider', 'get') );
		add_action( 'wp_ajax_nopriv_ecImage_provider', array('ecImageProvider', 'get') );
		add_action( 'image_size_names_choose', array('ecImageAdmin', 'imageSizeNamesChooseHook' ) );
		add_action( 'wp_get_attachment_metadata', array('ecImageAdmin', 'attachmentMetadataHook'), 10, 2);
		add_action( 'intermediate_image_sizes_advanced', array('ecImageAdmin', 'imageSizesAdvancedHook'), 1, 1);
		add_action( 'attachment_fields_to_edit', array('ecImageAdmin', 'editMediaCustomField'), 11, 2 );
		add_action( 'attachment_fields_to_save', array('ecImageAdmin', 'saveMediaCustomField'), 11, 2 );
		add_action( 'admin_enqueue_scripts', array('ecImageAdmin', 'enqueueScripts'));
		ecImageSettings::init();
		ecImageNotice::init();
	}
	
	public static function onInit () {
		$labels = array(
			'name' => ecImageI18n::get('name'),
			'singular_name' => ecImageI18n::get('singular_name')
		);
		$ecimage_size = array(
			'labels' => $labels,
			'public' => false,
			'publicly_queryable' => false,
			'show_ui' => true,
			'exclude_from_search' => true,
			'show_in_nav_menus' => false,
			'has_archive' => false,
			'rewrite' => false,
			'supports' => array('title')
		);
		register_post_type('ecimage_size', $ecimage_size);
	}
	
	public static function onActivation () {
		save_mod_rewrite_rules();
		wp_schedule_event(time(), 'daily', 'ecimage_license_cron');
	}
	
	public static function onDeactivation () {
		self::$deactivation = true;
		save_mod_rewrite_rules();
		self::clearCache();
		wp_clear_scheduled_hook( 'ecimage_license_cron' );
	}
	
	public static function getSizesList () {
		$sizes = array();
		
		foreach (ecImageDatabase::getSizeIDs() AS $sizeID) {
			$sizes[get_post_meta( $sizeID, 'ecimage-taxonomie', true )] = get_the_title( $sizeID );
		}
		
		return $sizes;
	}
	
	public static function updateHtaccess () {
		$htaccess = get_home_path().".htaccess";
		
		$lines = self::getHtaccessLines();
		
		return insert_with_markers($htaccess, "ecImage ".get_current_blog_id(), $lines);
	}
	
	public static function clearHtaccess () {
		$htaccess = get_home_path().".htaccess";
		
		return insert_with_markers($htaccess, "ecImage ".get_current_blog_id(), array());
	}
	
	public static function getHtaccessLines () {
		$lines = array();
		
		$lines[] = 'RewriteCond %{REQUEST_URI} ^' . ecImage_upload_uri . '.*';
		$lines[] = 'RewriteCond %{REQUEST_FILENAME} !-f';
		$lines[] = 'RewriteRule ' . ltrim(ecImage_upload_uri, '/') . '(.*/)?([^/]*)_([^_]*)\.(.*) /wp-admin/admin-ajax.php?action=ecImage_provider&d=$1&f=$2&s=$3&e=$4 [L]';
		
		$lines[] = 'RewriteCond %{REQUEST_URI} ^' . ecImage_upload_uri_base . '.*';
		$lines[] = 'RewriteCond %{REQUEST_FILENAME} !-f';
		$lines[] = 'RewriteRule ' . ltrim(ecImage_upload_uri_base, '/') . '(.*) ' . ecImage_upload_uri . '$1 [R=302,L]';
		
		return $lines;
	}
	
	public static function enqueueScripts () {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'ecimage-focus', ecImage_base_url.'js/focus.js', array( 'jquery' ) );
		wp_enqueue_style( 'ecimage-admin', ecImage_base_url.'css/admin.css' );
	}
	
	public static function clearCache ($sizeTax = false, $image = false) {
		$cacheDir = ecImageProvider::CACHE_PATH;
		if (empty($sizeTax)) {
			// delete all sizes
			$sizeTax = '*';
		}
		if (empty($image)) {
			// delete all images
			$image = '*';
		} else {
			$image = ltrim($image, '/');
		}
		$pattern = "{$image}_{$sizeTax}.*";
		$delCnt = self::delFiles($cacheDir, $pattern);
		return $delCnt;
	}

	public static function getTempDirSize () {
		$size = self::getDirSize(ecImageProvider::CACHE_PATH);
		return self::sizeFormat($size);
	}
	
	public static function sizeFormat ($size) {
		$i = 0;
		$unit = array('Byte', 'KB', 'MB', 'GB', 'TB');
		while ($size > 10000) {
			$size = round($size / 1000, 2);
			$i++;
		}
		return number_format($size, 2).' '.$unit[$i];
	}
	
	private static function getDirSize ($dir = '') {
		$size = 0;
		foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
			$size += is_file($each) ? filesize($each) : self::getDirSize($each);
		}
		return $size;
	}
	
	private static function delFiles ($dir, $pattern = '*.*') {
		$cnt = 0;
		foreach (glob(rtrim($dir, '/').'/'.$pattern, GLOB_NOSORT) as $file) {
			if (is_file($file) && unlink($file)) {
				$cnt++;
			}
		}
		foreach (glob(rtrim($dir, '/').'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $subDir) {
			if (is_dir($subDir)) {
				$cnt += self::delFiles($subDir,$pattern);
			}
		}
		return $cnt;
	}
	
	public static function registerImageSizes () {
		
		foreach (ecImageDatabase::getSizeIDs() AS $sizeID) {
			
			$size = new ecImageSize($sizeID);
			
			if (empty($size->ID)) {
				continue;
			}
			
			add_image_size( 'ecimage_'.$size->taxonomie, $size->width, $size->height, false);
			
		}
		
	}
	
	public static function imageSizeNamesChooseHook ( $sizes ) {
		
		$mySizes = array();
		
		foreach (self::getSizesList() AS $taxonomie => $label ) {
			$mySizes['ecimage_'.$taxonomie] = $label;
		}
		
		return array_merge($sizes, $mySizes);
		
	}
	
	public static function imageSizesAdvancedHook ($sizes) {
		
		foreach (self::getSizesList() AS $taxonomie => $label ) {
			unset($sizes['ecimage_'.$taxonomie]);
		}
		
		$overrides = get_option( 'ecimage_overrides', array() );
		
		foreach ($overrides AS $taxonomie => $override) {
			if (!empty($override['nogen'])) {
				unset($sizes[$taxonomie]);
			}
		}
		
		return $sizes;
		
	}
	
	public static function attachmentMetadataHook ($metadata, $attachmentID) {
		
		$mimetype = get_post_mime_type($attachmentID);
		
		// if not supported do nothing
		if (!in_array($mimetype, ecImageProvider::SUPPORTED_MIMETYPE)) {
			return $metadata;
		}
		
		// if sizes dont exist create it
		if (!isset($metadata['sizes'])) {
			$metadata['sizes'] = array();
		}
		
		$overrides = get_option( 'ecimage_overrides', array() );
		$o = array();
		foreach ($overrides AS $taxonomie => $override) {
			if (empty($override['override'])) {
				continue;
			}
			$o[$taxonomie] = intval($override['override']);
		}
		$sizes = ecImage::getSizes($attachmentID, false, $o);
		
		$metadata['sizes'] = array_merge($metadata['sizes'], $sizes);
		
		return $metadata;
		
	}
	
	public static function modRewriteRulesHook ($rules) {
		if (self::$deactivation) {
			return $rules;
		}
		$rules_arr = explode( "\n", $rules );
		$new = array();
		foreach ($rules_arr AS $r) {
			$new[] = $r;
			if ($r == 'RewriteRule ^index\.php$ - [L]') {
				$new = array_merge($new, self::getHtaccessLines());
			}
		}
		return implode("\n", $new);
	}
	
	public static function editMediaCustomField ($fields, $post) {
		if (!in_array($post->post_mime_type, ecImageProvider::SUPPORTED_MIMETYPE)) {
			return;
		}
		$focus = ecImageDatabase::getFocusPoint($post->ID);
		$fields['ecimage-focus'] = array(
			'label' => 'ec Image Focus',
			'input' => 'html',
			'html' => '<div class="ecimage-focus-form">'.
				'<p>'.ecImageI18n::get('label_focus_width').'</p>'.
				'<input type="range" name="attachments['.$post->ID.'][ecimage-focus][x]" value="'.$focus[0].'" min="0" max="100" step="1" class="ecimage-focus-x">'.
				'<p>'.ecImageI18n::get('label_focus_height').'</p>'.
				'<input type="range" name="attachments['.$post->ID.'][ecimage-focus][y]" value="'.$focus[1].'" min="0" max="100" step="1" class="ecimage-focus-y">'.
				'<p class="ecimage-crosshair-switch hidden"><label><input type="checkbox"> '.ecImageI18n::get('label_focus_crosshair').'</label></p>'.
				'<p class="help">'.ecImageI18n::get('help_focus').'</p>'.
				'<script>ecimageFocus.init();</script>'.
				'<div>'
		);
		
		return $fields;
	}
	
	public static function saveMediaCustomField ($post, $attachment) {
		
		if ( isset($attachment['ecimage-focus']) ) {
			$x = intval( $attachment['ecimage-focus']['x'] );
			$y = intval( $attachment['ecimage-focus']['y'] );
			$focus = array($x, $y);
			$focusOld = ecImageDatabase::getFocusPoint($post['ID']);
			update_post_meta( $post['ID'], 'ecimage-focus', $focus );
			if ($focusOld[0] != $focus[0] || $focusOld[1] != $focus[1]) {
				$media = wp_get_attachment_metadata($post['ID'], true);
				$pathinfo = pathinfo($media['file']);
				self::clearCache(false, $pathinfo['filename']);
			}
		}
		
		return $post;
	}
	
}