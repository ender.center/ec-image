<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ecImageNotice
 *
 * @author Felix
 */
class ecImageNotice {
	
	const OPTION_KEY = 'ecimage_notice';
	
	const NOTICE_PREFIX = '<b>ec Image:</b> ';
	
	public static $error = array();
	public static $warning = array();
	public static $info = array();
	public static $success = array();
	
	private static $userID;
	
	public static function init() {
		add_action( 'admin_notices', array('ecImageNotice', 'adminNotices'), 11 );
		add_action( 'init', array('ecImageNotice', 'initData'), 1);
	}
	
	public static function initData () {
		
		self::$userID = get_current_user_id();
		
		if (empty(self::$userID)) {
			return;
		}
		
		$data = get_user_meta(self::$userID, self::OPTION_KEY, true);
		
		if (!empty($data['error']) && is_array($data['error'])) {
			self::$error = $data['error'];
		}
		
		if (!empty($data['warning']) && is_array($data['warning'])) {
			self::$warning = $data['warning'];
		}
		
		if (!empty($data['info']) && is_array($data['info'])) {
			self::$info = $data['info'];
		}
		
		if (!empty($data['success']) && is_array($data['success'])) {
			self::$success = $data['success'];
		}
		
	}
	
	private static function saveData () {
		
		if (empty(self::$userID)) {
			return;
		}
		
		$data = array();
		$data['error'] = self::$error;
		$data['warning'] = self::$warning;
		$data['info'] = self::$info;
		$data['success'] = self::$success;
		update_user_meta(self::$userID, self::OPTION_KEY, $data);
		
	}
	
	public static function adminNotices () {
		
		foreach (self::$success AS $msg) {
			self::printNotice(self::NOTICE_PREFIX . $msg, 'success');
		}
		self::$success = array();
		
		foreach (self::$info AS $msg) {
			self::printNotice(self::NOTICE_PREFIX . $msg, 'info');
		}
		self::$info = array();
		
		foreach (self::$warning AS $msg) {
			self::printNotice(self::NOTICE_PREFIX . $msg, 'warning');
		}
		self::$warning = array();
		
		foreach (self::$error AS $msg) {
			self::printNotice(self::NOTICE_PREFIX . $msg, 'error');
		}
		self::$error = array();
		
		self::saveData();
		
	}
	
	public static function addError ($msg, $noEscape = false) {
		if ($noEscape) {
			self::$error[] = $msg;
		} else {
			self::$error[] = esc_html($msg);
		}
		self::saveData();
	}
	
	public static function addWarning ($msg, $noEscape = false) {
		if ($noEscape) {
			self::$warning[] = $msg;
		} else {
			self::$warning[] = esc_html($msg);
		}
		self::saveData();
	}
	
	public static function addInfo ($msg, $noEscape = false) {
		if ($noEscape) {
			self::$info[] = $msg;
		} else {
			self::$info[] = esc_html($msg);
		}
		self::saveData();
	}
	
	public static function addSuccess ($msg, $noEscape = false) {
		if ($noEscape) {
			self::$success[] = $msg;
		} else {
			self::$success[] = esc_html($msg);
		}
		self::saveData();
	}
	
	private static function printNotice ($msg, $type) {
		print "<div class='notice notice-$type'><p>$msg</p></div>";
	}
	
}
