<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ecImageSize
 *
 * @author Felix
 */

class ecImageSize {
	
	public static $post_type = 'ecimage_size';
	
	public $ID = null;
	public $label = null;
	public $taxonomie = null;
	public $width = null;
	public $height = null;
	public $scaling = null;
	
	function __construct($size) {
		
		if (is_numeric($size)) {
			$data = ecImageDatabase::getSizeByID($size);
		} elseif (is_string($size)) {
			$data = ecImageDatabase::getSizeByTaxonomie($size);
		} else {
			//$error = new WP_Error( 'ecImage Size not Valid', 'ecImage Size not Valid');
			return;
		}
		
		if (empty($data)) {
			self::getDefaults();
		}
		
		$this->ID = $data->ID;
		$this->label = $data->label;
		$this->taxonomie = $data->taxonomie;
		$this->width = $data->width;
		$this->height = $data->height;
		$this->scaling = $data->scaling;
		
	}
	
	public static function getDefaults () {
		
		$default = new stdClass();
		
		$default->label = '';
		$default->taxonomie = '';
		$default->width = 150;
		$default->height = 150;
		$default->scaling = 'contain';
		
		return $default;
		
	}
	
	public static function getCount () {
		return ecImageDatabase::getSizeCount();
	}
	
}
