<?php
/**
 * @package ec_image
 * @version 0.2.0
 */
/*
 * Plugin Name: ec Image
 * Text Domain: ec-image
 * Plugin URI: 
 * Description: Managing and Optimizing your Imagesizes easy, fast and on demand
 * Author: Felix Ender
 * Version: 0.1.2
 * Author URI: http://ender.center/
 */

if ( is_admin() ) {
	$uploadDir = wp_upload_dir();
} else {
	$uploadDir = wp_get_upload_dir();
}
$uploadDir['baseurl_parsed'] = parse_url($uploadDir['baseurl']);

define('ecImage_base_dir', plugin_dir_path(__FILE__));
define('ecImage_base_url', plugins_url( basename(dirname(__FILE__)) . '/' ));
define('ecImage_upload_dir', $uploadDir['basedir'] . '/ec-image/');
define('ecImage_tmp_dir', $uploadDir['basedir'] . '/ec-image/tmp/');

define('ecImage_upload_url', $uploadDir['baseurl'] . '/ec-image/');
define('ecImage_upload_uri_base', $uploadDir['baseurl_parsed']['path'] . '/');
define('ecImage_upload_uri', ecImage_upload_uri_base . 'ec-image/');

require_once(ecImage_base_dir . 'includes/autoloader.php');

ecImageAdmin::init();
