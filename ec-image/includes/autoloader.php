<?php

spl_autoload_register( 'ecImage_autoload' );

function ecImage_autoload( $class_name ) {
 
    // exit if not this Plugin.
    if ( false === strpos( $class_name, 'ecImage' ) || strpos( $class_name, 'ecImage' ) > 0 ) {
        return;
    }
	
	if (file_exists(ecImage_base_dir . "classes/$class_name.class.php")) {
		include_once(ecImage_base_dir . "classes/$class_name.class.php");
		return true;
	} else {
		return false;
	}
 
}