jQuery(function(){
	ecimageFocus = new function () {
		
		var crosshairHTML = '<div class="ecimage-crosshair-container"><div class="ecimage-crosshair-x"></div><div class="ecimage-crosshair-y"></div></div>';
		
		var firstTime = true;
		
		var elCont = null;
		var elX = null;
		var elY = null;
		
		var elImg = null;
		
		var inputX = null;
		var inputY = null;
		
		var onCrosshairSwitch = function (ev) {
			ev.preventDefault();
			if (this.checked) {
				crosshairShow();
			} else {
				crosshairHide();
			}
			return false;
		};
		
		var onInputChange = function () {
			setCrosshairPos();
			submitForm();
		};
		
		var crosshairShow = function () {
			elImg.before(crosshairHTML);
			elCont = elImg.parent().addClass('ecimage-has-crosshair').find('.ecimage-crosshair-container');
			elX = elCont.find('.ecimage-crosshair-x');
			elY = elCont.find('.ecimage-crosshair-y');
			setCrosshairPos();
			elCont.on('click',setClickPos);
			elImg.on('resize',setCrosshairPos);
		};
		
		var setCrosshairPos = function () {
			
			if (elImg === null || elCont === null || elX === null || elY === null || inputX === null || inputY === null) {
				return false;
			}
			
			elCont.css('left', (elImg.offset().left - elImg.parent().offset().left)+'px');
			elCont.css('top', (elImg.offset().top - elImg.parent().offset().top)+'px');
			elCont.width(elImg.width()+'px');
			elCont.height(elImg.height()+'px');
			
			elX.css('left', elCont.width() / 100 * inputX.val());
			elY.css('top', elCont.height() / 100 * inputY.val());
			
		};
		
		var setClickPos = function (ev) {
			inputX.val((ev.clientX - elCont.offset().left) / elCont.width() * 100);
			inputY.val((ev.clientY - elCont.offset().top) / elCont.height() * 100);
			inputX.change();
			submitForm();
		};
		
		var crosshairHide = function () {
			elCont.remove();
			elCont = null;
			elX = null;
			elY = null;
		};
		
		var submitForm = function () {
			inputX.parents('form').submit();
		};
		
		var hide = function () {
			reset();
			jQuery('.compat-field-ecimage-focus').addClass('hidden');
		};
		
		var reset = function () {
			elCont = null;
			elX = null;
			elY = null;
			elImg = null;
			inputX = null;
			inputY = null;
		};
		
		this.init = function () {
			reset();
			elImg = jQuery('.edit-attachment-frame .attachment-media-view .details-image');
			if (elImg.length === 0) {
				hide();
				return;
			}
			inputX = jQuery('.ecimage-focus-form input.ecimage-focus-x').on('change',onInputChange);
			inputY = jQuery('.ecimage-focus-form input.ecimage-focus-y').on('change',onInputChange);
			jQuery('.ecimage-crosshair-switch').removeClass('hidden');
			jQuery('.ecimage-crosshair-switch input').on('change', onCrosshairSwitch);
			if (firstTime) {
				jQuery(window).on('resize', setCrosshairPos);
			} else if (elImg.parent().is('.ecimage-has-crosshair')) {
				elCont = elImg.parent().find('.ecimage-crosshair-container');
				elX = elCont.find('.ecimage-crosshair-x');
				elY = elCont.find('.ecimage-crosshair-y');
				jQuery('.ecimage-crosshair-switch input').attr('checked', 'checked');
			}
			firstTime = false;
		};
		
	};
});